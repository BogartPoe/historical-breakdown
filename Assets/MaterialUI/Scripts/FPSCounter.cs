﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace MaterialUI
{
	public class FPSCounter : MonoBehaviour
	{
		public float updateInterval = 0.5f;

		float deltaFps = 0f; 
		int frames = 0; 
		float timeleft; 

		public Text theText;
		
		void Start()
		{
			timeleft = updateInterval;  
		}
		
		void Update()
		{
			timeleft -= Time.deltaTime;
			deltaFps += Time.timeScale/Time.deltaTime;
			++frames;
			
			
			if( timeleft <= 0f )
			{
				
				theText.text = "" + (deltaFps/frames).ToString("f2") + " FPS";
				if ((deltaFps/frames) < 1)
				{
					theText.text = "";
				}
				timeleft = updateInterval;
				deltaFps = 0f;
				frames = 0;
			}
		}
	}
}