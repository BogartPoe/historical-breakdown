﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLoader : MonoBehaviour {


    private bool playerInTheZone;

    public string levelToload;

	// Use this for initialization
	void Start () {
        playerInTheZone = false;	
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.UpArrow) && playerInTheZone)
        {
            
            Application.LoadLevel(levelToload);
        }
		
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player")
        {
            playerInTheZone = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.name == "Player")
        {
            playerInTheZone = false;
        }
    }
}
