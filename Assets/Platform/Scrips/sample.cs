﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class sample : MonoBehaviour {

    public Text countText;
    
    public float speed;	
    private Rigidbody2D rb2d;
    private int count;

    public GameObject coinCollect;
    public GameObject popText;

    void Start()
    {
        
        rb2d = GetComponent<Rigidbody2D>();
        count = 0;
        
        SetCountText();
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis ("Horizontal");
        float moveVertical = Input.GetAxis ("Vertical");
        Vector2 movement = new Vector2(moveHorizontal, moveVertical);
        rb2d.AddForce(movement * speed);
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            Instantiate(coinCollect, other.transform.position, other.transform.rotation);
   
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
            
            
        }
    }
    void SetCountText()
    {
        
        countText.text = "Coin : " + count.ToString() +"/6";

       
    }
}


